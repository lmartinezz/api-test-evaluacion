package com.springboot.api.gradle.service.impl;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.gradle.dao.impl.ClienteDaoImpl;
import com.springboot.api.gradle.model.Cliente;
import com.springboot.api.gradle.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService{

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ClienteDaoImpl _clienteDao;
	@Override
	public Map<String, Object> getAllClientes() {
		// TODO Auto-generated method stub
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("codigo_servicio", "0000");
		response.put("clientes", _clienteDao.getAllClientes());
		return  response;
	}

	@Override
	public Map<String, Object> getCliente(Integer id) {
		// TODO Auto-generated method stub
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("codigo_servicio", "0000");
		response.put("cliente", _clienteDao.getCliente(id));
		return response;
	}

	@Override
	public Map<String, String> saveCliente(Cliente cliente) {
		// TODO Auto-generated method stub
		Map<String, String> response = new HashMap<String, String>();
		try {
			_clienteDao.saveCliente(cliente);
			response.put("codigo_servicio", "0000");
			response.put("descripcion", "Cliente registrado con éxito");
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.put("codigo_servicio","0001");
			response.put("descripcion", e.getMessage());
		}
		return response;
	}

	@Override
	public Map<String, String> deleteCliente(Integer id) {
		// TODO Auto-generated method stub
		Map<String, String> response = new HashMap<String, String>();
		try {
			_clienteDao.deleteCliente(id);
			response.put("codigo_servicio", "0000");
			response.put("descripcion", "Cliente eliminado con éxito");
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.put("codigo_servicio","0001");
			response.put("descripcion", e.getMessage());
		}
		return response;
	}

}
