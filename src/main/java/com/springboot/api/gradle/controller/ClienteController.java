package com.springboot.api.gradle.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Cliente;
import com.springboot.api.gradle.service.impl.ClienteServiceImpl;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	@Autowired
	private ClienteServiceImpl _clienteService;
	
	@GetMapping(value = "/", produces = "application/json")	
	public Map<String, Object> getAllClientes(){
		return _clienteService.getAllClientes();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Map<String, Object> getCliente(@PathVariable ("id") Integer id){
		return _clienteService.getCliente(id);
	}
	
	@PostMapping(value = "/", produces = "application/json")	
	public Map<String, String> saveCliente(@RequestBody Cliente cliente){
		return _clienteService.saveCliente(cliente);
	}	
	
	@DeleteMapping(value = "/{id}", produces = "application/json")	
	public Map<String, String> deleteCliente(@PathVariable ("id") Integer id){
		
		return _clienteService.deleteCliente(id);
	}	

}
